
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },
  {
    path: '/Users',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: 'list', component: () => import('pages/Users/list.vue') },
      { path: 'create', component: () => import('pages/Users/create.vue') },
      { path: 'update/:id', component: () => import('pages/Users/update.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
